package com.shusharin.fanfictiontomorrow.utils

import com.shusharin.fanfictiontomorrow.requests.utils.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface RetrofitServices {
    @GET("present_book_in_genre")
    fun getPresentBookInGenre(): Call<ListPresentBookInGenre>

    @GET("library_book")
    fun getLibraryBook(
        @Query("id_user") idUser: Int,
        @Query("id_book") idBook: Int,
    ): Call<LibraryBook>

    @GET("book")
    fun getBook(
        @Query("id_user") idUser: Int,
        @Query("id_book") idBook: Int,
    ): Call<BookClass>

    @GET("main_page_book")
    fun getMainPageBook(@Query("id_book") idBook: Int): Call<MainPageBook>

    @GET("find_book")
    fun getSearchBook(
        @Query("id_book") idBook: Int,
        @Query("sort_type") sortType: Int,
    ): Call<SearchBook>

    @GET("set_draft")
    fun setDraft(
        @Query("id_book") idBook: Int,
    ): Call<Boolean>

    @GET("user")
    fun getUser(@Query("id_user") idUser: Int): Call<Person>

    @GET("chapters")
    fun getChapters(@Query("id_book") idBook: Int): Call<ListChapterInBook>

    @GET("chapter")
    fun getChapter(
        @Query("id_user") idUser: Int,
        @Query("id_chapter") idChapter: Int,
    ): Call<ChapterClass>

    @GET("my_library")
    fun getLibrary(
        @Query("id_user") idUser: Int,
    ): Call<ListBookId>

    @GET("my_write_book")
    fun getWriteBook(
        @Query("id_user") idUser: Int,
    ): Call<ListBookId>

    @GET("book_with_status")
    fun getBooksWithStatus(
        @Query("id_user") idUser: Int,
        @Query("id_status") idStatus: Int,
    ): Call<ListBookId>

    @GET("book_in_genre")
    fun getGenreBook(
        @Query("id_genre") idGenre: Int,
    ): Call<ListBookId>

    @GET("book_in_section")
    fun getBooksInSection(
        @Query("id_section") idSection: Int,
    ): Call<ListBookId>

    @POST("get_auth_user_id")
    fun getAuthUserId(
        @Query("first_name") name: String,
        @Query("last_name") surname: String,
        @Query("email") googleAuth: String,
    ): Call<Int>

    @GET("get_last_books")
    fun getLastBooks(
        @Query("id_user") idUser: Int,
    ): Call<ListBookId>

    @GET("get_last_chapter")
    fun getLastChapter(
        @Query("id_user") idUser: Int,
        @Query("id_book") idBook: Int,
    ): Call<Int>

    @GET("add_book_in_library")
    fun addBookInLibrary(
        @Query("id_user") idUser: Int,
        @Query("id_status") idStatus: Int,
        @Query("id_book") idBook: Int,
    ): Call<Boolean>

    @Multipart
    @POST("add_cover")
    fun addCover(@Part image: MultipartBody.Part): Call<Int>

    @Multipart
    @POST("change_cover")
    fun changeCover(
        @Query("id_cover") idCover: Int,
        @Part image: MultipartBody.Part,
    ): Call<Boolean>

    @GET("get_cover")
    fun getCover(
        @Query("id_cover") idCover: Int,
    ): Call<ResponseBody>

    @Multipart
    @POST("add_book")
    fun addBook(@Part("book") book: RequestBody): Call<Int>

    @Multipart
    @POST("change_book")
    fun changeBook(
        @Query("id_book") idBook: Int,
        @Part("book") book: RequestBody,
    ): Call<Int>

    @Multipart
    @POST("add_chapters")
    fun addChapter(
        @Query("id_book") idBook: Int,
        @Part("chapter") chapter: RequestBody,
    ): Call<Int>

    @Multipart
    @POST("change_chapter")
    fun changeChapter(
        @Query("id_chapter") idChapter: Int,
        @Part("chapter") book: RequestBody,
    ): Call<Boolean>

    @GET("delete_chapter")
    fun deleteChapter(@Query("id_chapter") idChapter: Int): Call<Boolean>

    @Multipart
    @POST("search")
    fun search(
        @Part("search_params") book: RequestBody,
    ): Call<ListBookId>

    @Multipart
    @POST("library_search")
    fun librarySearch(
        @Part("search_params") book: RequestBody,
        @Query("id_user") idUser: Int,
    ): Call<ListBookId>
    //library_search
}