package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.annotation.JsonProperty

class MainPageBook {
    @JsonProperty
    var name = ""

    @JsonProperty
    var authorId = 0

    @JsonProperty
    var coverId = 0

    @JsonProperty
    var isDraft = false

    constructor() {}
    constructor(name: String, authorId: Int, coverId: Int, isDraft: Boolean) {
        this.name = name
        this.authorId = authorId
        this.coverId = coverId
        this.isDraft = isDraft
    }
}