package com.shusharin.fanfictiontomorrow.requests.utils.classes

import com.shusharin.fanfictiontomorrow.R

enum class StatusCreatedBooks(val id: Int, val nameSCB: Int, val idColor: Int) {
    COMPLETED(1, R.string.status_books_completed, R.color.special_2),
    IN_PROCESS(2, R.string.status_books_in_process, R.color.author);

    companion object {
        fun getStatusBook(idStatusBooks: Int): StatusCreatedBooks {
            return values()[idStatusBooks - 1]
        }
    }
}