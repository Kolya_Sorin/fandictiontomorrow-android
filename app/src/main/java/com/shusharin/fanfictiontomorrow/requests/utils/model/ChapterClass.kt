package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.annotation.JsonProperty

class ChapterClass {
    @JsonProperty
    var chapterId = 0

    @JsonProperty
    var text = ""

    constructor() {}
    constructor(chapterId: Int, text: String) {
        this.chapterId = chapterId
        this.text = text
    }
}