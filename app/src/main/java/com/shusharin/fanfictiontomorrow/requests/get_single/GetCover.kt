package com.shusharin.fanfictiontomorrow.requests.get_single

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.MyRequest
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import okhttp3.ResponseBody
import retrofit2.Call

class CoverParams(val idCover: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "CoverParams(idCover=$idCover)"
    }
}

class GetCover(
    val resources: Resources,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) :
    MyRequest<ResponseBody, CoverParams, Bitmap>(startRefresh, stopRefresh) {
    val liveBitmap: MutableLiveData<Bitmap>
        get() = liveData

    fun sendRequest(idCover: Int) {
        sendRequest(CoverParams(idCover))
    }

    override fun analysisOfResponseBody(responseBody: ResponseBody, params: CoverParams) {
        var bmp = BitmapFactory.decodeStream(responseBody.byteStream())

        if (bmp == null) {
            bmp = BitmapFactory.decodeResource(
                resources,
                R.drawable.empty
            )
        }
        setLiveData(bmp)
    }

    override fun call(params: CoverParams): Call<ResponseBody> {
        return service.getCover(params.idCover)
    }
}