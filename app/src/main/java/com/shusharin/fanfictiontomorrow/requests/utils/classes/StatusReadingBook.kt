package com.shusharin.fanfictiontomorrow.requests.utils.classes

import com.shusharin.fanfictiontomorrow.R

enum class StatusReadingBook(
    val id: Int,
    val idIcon: Int,
    val nameSRB: String,
) {
    READING(1, R.drawable.icon_reading, "Читаю"),
    READ(2, R.drawable.icon_read, "Прочитанно"),
    NOT_INTERESTING(3, R.drawable.icon_not_intresting, "Не интересно"),
    READ_LATER(4, R.drawable.icon_read_later, "Прочитаю позже"),
    NOT_IN_LIBRARY(5, R.drawable.icon_read_later, "В библиотеку"),
    MY(6, R.drawable.icon_read_later, "Пишу");

    companion object {
        fun getStatusReadingBook(idLists: Int): StatusReadingBook {
            return values()[idLists - 1]
        }
    }
}