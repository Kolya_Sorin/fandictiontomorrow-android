package com.shusharin.fanfictiontomorrow.requests.get_queue

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequest
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.model.ChapterClass
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import retrofit2.Call

class ChapterParams(val idChapter: Int, val idUser: Int = MainActivity.idUser) :
    MyRequestParams() {
    override fun toString(): String {
        return "ChapterParams(idChapter=$idChapter, idUser=$idUser)"
    }
}

class GetChapter(startRefresh: () -> Unit, stopRefresh: () -> Unit) :
    MyRequest<ChapterClass, ChapterParams, Chapter>(startRefresh, stopRefresh) {
    val liveChapter: MutableLiveData<Chapter>
        get() = liveData

    fun sendRequest(idChapter: Int, idUser: Int = MainActivity.idUser) {
        sendRequest(ChapterParams(idChapter, idUser))
    }

    override fun analysisOfResponseBody(
        responseBody: ChapterClass,
        params: ChapterParams,
    ) {
        setLiveData(Chapter(responseBody))
    }

    override fun call(params: ChapterParams): Call<ChapterClass> {
        return service.getChapter(params.idUser, params.idChapter)
    }
}