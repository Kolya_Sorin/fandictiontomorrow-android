package com.shusharin.fanfictiontomorrow.requests.utils.model

data class Person(var id: Int) {
    constructor() : this(-1)
    constructor(id: Int, name: String, surname: String) : this(id) {
        this.name = name
        this.surname = surname
    }

    var name: String = ""
    var surname: String = ""
    override fun toString(): String {
        return "$name $surname"
    }


}