package com.shusharin.fanfictiontomorrow.requests.utils.classes

import com.shusharin.fanfictiontomorrow.R

enum class Sections(val idSection: Int, val nameSection: Int) {
    POPULATE(1, R.string.sections_populate),
    HOT_NEW_ITEMS(2, R.string.sections_hot_new_items),
    LAST(3, R.string.sections_last);

    companion object {
        fun get(idSection: Int): Sections {
            return values()[idSection - 1]
        }
    }
}