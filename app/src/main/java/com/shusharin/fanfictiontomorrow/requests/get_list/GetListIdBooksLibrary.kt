package com.shusharin.fanfictiontomorrow.requests.get_list

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetLibraryBook
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.ReadBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class ListIdBooksLibraryParams(val idUser: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "ListIdBooksLibraryParams(idUser=$idUser)"
    }
}

class GetListIdBooksLibrary(
    override val owner: LifecycleOwner, override val api: MyApiServices,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : GetListIdBook<ListIdBooksLibraryParams, ReadBook>(
    startRefresh,
    stopRefresh
), IGetLibraryBook {
    val liveLibraryBooks: MutableLiveData<HashMap<Int, ReadBook>>
        get() = liveData

    fun sendRequest(idUser: Int) {
        data.clear()
        sendRequest(ListIdBooksLibraryParams(idUser))
    }

    override fun analysisOfResponseBody(
        responseBody: ListBookId,
        params: ListIdBooksLibraryParams,
    ) {
        println(responseBody.booksId)
        getLibraryBook(params.idUser, responseBody)
    }

    override fun call(params: ListIdBooksLibraryParams): Call<ListBookId> {
        return service.getLibrary(params.idUser)
    }

    override fun setLibraryBook(it: ReadBook) {
        setLiveData(it)
    }
}