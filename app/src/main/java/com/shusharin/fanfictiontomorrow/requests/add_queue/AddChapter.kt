package com.shusharin.fanfictiontomorrow.requests.add_queue

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.model.ChapterSmall
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call

class AddChapterParams(val idBook: Int, val chapterSmall: ChapterSmall) : MyRequestParams() {
    override fun toString(): String {
        return "AddChapterParams(idBook=$idBook, chapterSmall=$chapterSmall)"
    }
}

class AddChapter(startRefresh: () -> Unit, stopRefresh: () -> Unit) :
    MyRequestIdentity<Int, AddChapterParams>(startRefresh, stopRefresh) {
    val liveIdChapter: MutableLiveData<Int>
        get() = liveData

    fun sendRequest(idBook: Int, chapter: Chapter) {
        sendRequest(AddChapterParams(idBook, ChapterSmall(chapter.name, chapter.text)))
    }

    override fun call(params: AddChapterParams): Call<Int> {
        val requestBody = RequestBody.create(
            MediaType.get("application/json; charset=utf-8"), params.chapterSmall.toString()
        )
        return service.addChapter(params.idBook, requestBody)
    }


}