package com.shusharin.fanfictiontomorrow.requests.get_list

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetSearchBook
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class ListIdGenreBooksParams(val idGenre: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "ListIdGenreBooksParams(idGenre=$idGenre)"
    }
}

class GetListIdGenreBooks(
    override val owner: LifecycleOwner, override val api: MyApiServices,
    startRefresh: () -> Unit,
    stopRefresh: () -> Unit,
) : GetListIdBook<ListIdGenreBooksParams, FindBook>(
    startRefresh,
    stopRefresh
),
    IGetSearchBook {
    val liveSearchInGenreBook: MutableLiveData<HashMap<Int, FindBook>>
        get() = liveData

    fun sendRequest(idGenre: Int) {
        data.clear()
        sendRequest(ListIdGenreBooksParams(idGenre))
    }

    override fun analysisOfResponseBody(responseBody: ListBookId, params: ListIdGenreBooksParams) {
        println(responseBody.booksId)
        getSearchBook(idUser, responseBody, 1)
    }

    override fun call(params: ListIdGenreBooksParams): Call<ListBookId> {
        return service.getGenreBook(params.idGenre)
    }

    override fun setSearchBook(it: FindBook) {
        setLiveData(it)
    }
}