package com.shusharin.fanfictiontomorrow.ui.find_book.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.main_page.adapters.AdapterSmallBooks

class AdapterFindBook(private val books: HashMap<Int, FindBook>) :
    RecyclerView.Adapter<AdapterFindBook.FindBookViewHolder>() {

    open class FindBookViewHolder(itemView: View) :
        AdapterSmallBooks.SmallBookViewHolder(itemView) {
        val nameGenre: TextView = itemView.findViewById(R.id.name_genre)
        val annotate: TextView = itemView.findViewById(R.id.annotate)
        val quantityOfViews: TextView = itemView.findViewById(R.id.quantityOfViews)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FindBookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.content_find_book, parent, false)
        return FindBookViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FindBookViewHolder, position: Int) {
        val temp = books.toList().toTypedArray()
        temp.sortBy { it.second.sort }
        val book = temp[position].second
        holder.coverBook.setImageBitmap(book.bitmap)

        holder.nameBook.text = book.name
        holder.nameAuthor.text = book.author.toString()
        holder.cornerCon.clipToOutline = true

        holder.coverBook.clipToOutline = true
        holder.nameGenre.text = holder.itemView.context.getString(book.genre.nameG)
        holder.annotate.text = book.annotate
        holder.quantityOfViews.text = book.quantityOfViews.toString()

        holder.cornerCon.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                holder.itemView.context.getString(R.string.id_book),
                book.id
            )
            MainActivity.navigate(R.id.nav_book, bundle)
        }
    }

    override fun getItemCount(): Int {
        return books.size
    }
}