package com.shusharin.fanfictiontomorrow.ui.main_page.adapters

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.utils.WHERE_FIND

class AdapterSection(private val sections: HashMap<Sections, HashMap<Int, FindBook>>) :
    RecyclerView.Adapter<AdapterSection.ChapterViewHolder>() {

    class ChapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameChapter: TextView = itemView.findViewById(R.id.nameBook)
        val buttonAll: Button = itemView.findViewById(R.id.buttonAll)
        val listBooks: RecyclerView = itemView.findViewById(R.id.listBooks)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChapterViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.content_main_page_chapter_list, parent, false)
        return ChapterViewHolder(itemView)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ChapterViewHolder, position: Int) {
        val temp = sections.toList().toTypedArray()
        temp.sortBy { it.first.idSection }
//        println(temp.contentToString())
        val section = temp[position]
        holder.nameChapter.text = holder.itemView.context.getString(section.first.nameSection)
        if (section.first.idSection == Sections.LAST.idSection) {
            holder.buttonAll.visibility = INVISIBLE
        }
        holder.listBooks.adapter = AdapterSmallBooks(section.second)
        holder.listBooks.adapter!!.notifyDataSetChanged()
        holder.buttonAll.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                holder.itemView.context.getString(R.string.where_find),
                WHERE_FIND.SECTION.id
            )
            bundle.putInt(
                holder.itemView.context.getString(R.string.id_section),
                section.first.idSection
            )
            MainActivity.navigate(R.id.nav_find_book, bundle)
        }
    }

    override fun getItemCount() = sections.size
}