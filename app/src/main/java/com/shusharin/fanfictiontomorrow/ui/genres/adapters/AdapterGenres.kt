package com.shusharin.fanfictiontomorrow.ui.genres.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genre
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.utils.WHERE_FIND

class AdapterGenres(private val genres: List<Genre>) :
    RecyclerView.Adapter<AdapterGenres.GenresViewHolder>() {

    open class GenresViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cornerCon: View = itemView.findViewById(R.id.cornerCon)
        val imageGenre: ImageView = itemView.findViewById(R.id.imageGenre)
        val nameGenre: TextView = itemView.findViewById(R.id.name_genre)
        val view: View = itemView.findViewById(R.id.isNotClicable)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenresViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.content_genre, parent, false)
        return GenresViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: GenresViewHolder, position: Int) {
        val genre: Genres = Genres.values()[genres[position].genre - 1]
        holder.imageGenre.setImageResource(genre.idImage)
        holder.cornerCon.clipToOutline = true
        holder.nameGenre.text = holder.view.context.getString(genre.nameG)
        if (!genres[position].isPresent) {
            holder.cornerCon.isClickable = false
            holder.view.visibility = View.VISIBLE
        } else {
            holder.cornerCon.isClickable = true
            holder.view.visibility = View.INVISIBLE
            holder.cornerCon.setOnClickListener {
                val bundle = Bundle()
                bundle.putInt(
                    holder.itemView.context.getString(R.string.where_find),
                    WHERE_FIND.GENRE.id
                )
                bundle.putInt(
                    holder.itemView.context.getString(R.string.id_genre),
                    genre.id
                )
                MainActivity.navigate(R.id.nav_find_book, bundle)
//            Navigation.createNavigateOnClickListener(R.id.nav_find_book, bundle)
            }
        }
    }

    override fun getItemCount() = genres.size
}