package com.shusharin.fanfictiontomorrow.ui.chapters

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.chapters.adapters.AdapterChapters
import com.shusharin.fanfictiontomorrow.ui.parents.RecycleViewFragment

class ChaptersFragment :
    RecycleViewFragment<ChaptersViewModel, AdapterChapters.FindBookViewHolder, AdapterChapters>() {

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<ChaptersViewModel>()
        viewModel.idBook = arguments?.getInt(getString(R.string.id_book))!!

        viewModel.liveIdLastChapter.observe(viewLifecycleOwner) { idLastChapter ->
            viewModel.idLastChapter = idLastChapter
            adapter?.idLastChapter = idLastChapter
            adapter?.notifyDataSetChanged()
        }

        update()
        refresh()
        return getRoot1()
    }

    private fun getRoot1() = binding.root

    override fun update() {
        viewModel.getListIdChapters.sendRequest(viewModel.idBook)
        viewModel.getLastChapter.sendRequest(idBook = viewModel.idBook)
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(
            MainActivity.Companion.Nav(
                R.id.action_nav_book_to_chaptersFragment,
                arguments
            )
        )
    }

    override fun setAdapter() {
        viewModel.liveChapters.observe(viewLifecycleOwner) { chapters ->
            adapter = AdapterChapters(chapters, viewModel.idBook)
            adapter!!.idLastChapter = viewModel.idLastChapter
        }

        list!!.setHasFixedSize(true)
    }
}