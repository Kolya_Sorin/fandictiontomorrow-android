package com.shusharin.fanfictiontomorrow.ui.utils.menu_providers

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.core.view.MenuProvider
import androidx.navigation.ui.onNavDestinationSelected
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogSearch

class MenuProviderGeneral(private val dialogSearch: DialogSearch, val context: Context) :
    MenuProvider {
    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menu.clear()
        menuInflater.inflate(R.menu.app_bar_main_page, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.nav_my_profile -> {
                if (MainActivity.idUser == -1) {
                    MainActivity.createNeedLoginDialog(context).show()
                } else {
                    val bundle = Bundle()
                    bundle.putInt(
                        context.getString(R.string.id_user), -1
                    )
                    MainActivity.navigate(R.id.nav_my_profile, bundle)
                }
                return true
            }

            R.id.app_bar_search -> dialogSearch.show(
                dialogSearch.supportFragmentManager.beginTransaction(),
                "dialogSorting"
            )

            else -> return menuItem.onNavDestinationSelected(MainActivity.navController)
        }

        return menuItem.onNavDestinationSelected(MainActivity.navController)
    }
}