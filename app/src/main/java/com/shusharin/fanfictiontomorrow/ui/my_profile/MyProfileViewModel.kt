package com.shusharin.fanfictiontomorrow.ui.my_profile

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices


class MyProfileViewModel(api: MyApiServices) : ViewModelUpdatable(api){
    val getUser = api.getUser()
    val person = getUser.livePerson
}