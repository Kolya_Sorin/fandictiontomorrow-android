package com.shusharin.fanfictiontomorrow.ui.utils

enum class WHERE_FIND(val id: Int) {
    SECTION(0),
    GENRE(1),
    FIND(2)
}