package com.shusharin.fanfictiontomorrow.ui.utils.dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.shusharin.fanfictiontomorrow.R


class DialogYesNo<TypeAddParam>(
    private val title: String,
    private val listener: MyDialogYesNoInterface<TypeAddParam>,
    private val param: TypeAddParam? = null,
) :
    DialogFragment() {

    interface MyDialogYesNoInterface<TypeAddParam> {
        abstract val mySupportFragmentManager: FragmentManager

        fun onYes(param: TypeAddParam? = null)
        fun onNo(param: TypeAddParam? = null)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(title)
                .setPositiveButton(getString(R.string.dialog_yes)) { _, _ ->
                    listener.onYes(param)
                }
                .setNegativeButton(
                    getString(R.string.dialog_no)
                ) { _, _ ->
                    listener.onNo(param)
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}