package com.shusharin.fanfictiontomorrow.ui.genres

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.genres.adapters.AdapterGenres
import com.shusharin.fanfictiontomorrow.ui.parents.RecycleViewFragment


class GenresFragment :
    RecycleViewFragment<GenresViewModel, AdapterGenres.GenresViewHolder, AdapterGenres>() {
    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<GenresViewModel>()
        MainActivity.navController = findNavController()
        update()
        refresh()
        return binding.root
    }

    override fun update() {
        viewModel.getPresentBookInGenre.sendRequest()
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(
            MainActivity.Companion.Nav(R.id.nav_genres, arguments)
        )
    }

    override fun setAdapter() {
        viewModel.genres.observe(this) {
            adapter = AdapterGenres(it)
        }
        list!!.setHasFixedSize(true)
    }


}