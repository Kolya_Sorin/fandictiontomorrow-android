package com.shusharin.fanfictiontomorrow.ui.my_works.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.my_library.adapters.AdapterReedBooks

class AdapterWorkBooks(private val workBooks: HashMap<Int, Book>) :
    RecyclerView.Adapter<AdapterWorkBooks.WorkBookViewHolder>() {

    class WorkBookViewHolder(itemView: View) : AdapterReedBooks.ReadBookViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkBookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.content_read_book, parent, false)
        return WorkBookViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WorkBookViewHolder, position: Int) {
        val temp = workBooks.toList().toTypedArray()
        temp.sortBy { it.first }
        val workBook = temp[position].second

        holder.coverBook.setImageBitmap(workBook.bitmap)
        holder.nameBook.text = workBook.name
        holder.nameAuthor.text = workBook.author.toString()
        holder.cornerCon.clipToOutline = true

        holder.cornerCon.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                holder.itemView.context.getString(R.string.id_book),
                workBook.id
            )
            MainActivity.navigate(R.id.nav_book, bundle)
        }

        holder.coverBook.clipToOutline = true
        holder.status.visibility = View.INVISIBLE
    }

    override fun getItemCount() = workBooks.size
}