package com.shusharin.fanfictiontomorrow.requests.get_list

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListChapterInBook
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetListIdChaptersTest : RequestTestBase() {
    private lateinit var mockRequest: GetListIdChapters

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getListIdChapters)
    }

    @Test
    fun sendRequestFake() {
        val listBookId = ListChapterInBook()
        listBookId.chapters = arrayListOf(chapterInBookDefaultResponse)
        val listIdParams = ListIdChaptersParams(1)
        sendRequest(mockRequest, listIdParams, listBookId)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val chapter = slotHashMapChapter.captured[bookDefaultResponse.id]!!
        println(chapter)
        assert(chapter.id == chapterInBookDefaultResponse.chapterId)
        assert(chapter.name == chapterInBookDefaultResponse.name)
        assert(chapter.numberInBook == chapterInBookDefaultResponse.numberInBook)
    }
}