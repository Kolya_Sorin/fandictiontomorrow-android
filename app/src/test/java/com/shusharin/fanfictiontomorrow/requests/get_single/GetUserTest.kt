package com.shusharin.fanfictiontomorrow.requests.get_single

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetUserTest : RequestTestBase() {
    private lateinit var mockRequest: GetUser

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getUser)
    }

    @Test
    fun sendRequestFake() {
        val person = Person(1, "Андрей", "Шушарин")
        sendRequest(mockRequest, UserParams(1), person)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val personResponse = slotPerson.captured
        assert(person.id == personResponse.id)
        assert(person.name == personResponse.name)
        assert(person.surname == personResponse.surname)
    }
}