package com.shusharin.fanfictiontomorrow.requests.change

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class ChangeChapterTest : RequestTestBase() {
    private lateinit var mockRequest: ChangeChapter

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::changeChapter)
    }

    @Test
    fun sendRequestFake() {
        val result = true
        val changeChapterParams = mockk<ChangeChapterParams>()
        sendRequest(mockRequest, changeChapterParams, result)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val resultResponse = slotBoolean.captured
        assert(resultResponse == result)
    }
}