package com.shusharin.fanfictiontomorrow.requests.get_single

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetAuthUserIdTest : RequestTestBase() {
    private lateinit var mockRequest: GetAuthUserId

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getAuthUserId)
    }

    @Test
    fun sendRequestFake() {
        sendRequest(mockRequest, mockk(), 1)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val lastChapter = slotIntGetAuthUserId.captured
        slotIntGetAuthUserId.clear()
        assert(lastChapter == 1)
    }
}