package com.shusharin.fanfictiontomorrow.requests.add_single

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class AddBookInLibraryTest : RequestTestBase() {
    private lateinit var mockRequest: AddBookInLibrary

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::addBookInLibrary)
    }

    @Test
    fun sendRequestFake() {
        val isBookAdded = true
        val bookInLibraryParams = mockk<BookInLibraryParams>()
        sendRequest(mockRequest, bookInLibraryParams, isBookAdded)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val isBookAddedResponse = slotBoolean.captured
        assert(isBookAddedResponse == isBookAdded)
    }
}