package com.shusharin.fanfictiontomorrow.requests.get_queue

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.model.ChapterClass
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetChapterTest : RequestTestBase() {
    private lateinit var mockRequest: GetChapter

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getChapter)
    }

    @Test
    fun sendRequestFake() {
        val chapterClass = ChapterClass(1, "Название")
        val chapterParams = mockk<ChapterParams>()
        sendRequest(mockRequest, chapterParams, chapterClass)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val chapter = slotChapter.captured

        assert(chapter.id == chapterClass.chapterId)
        assert(chapter.text == chapterClass.text)
    }
}