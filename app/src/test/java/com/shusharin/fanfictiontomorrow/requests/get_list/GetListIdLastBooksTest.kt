package com.shusharin.fanfictiontomorrow.requests.get_list

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetListIdLastBooksTest : RequestTestBase() {
    private lateinit var mockRequest: GetListIdLastBooks

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getListIdLastBooks)
        isMockAll = false
        mockRequest = mockkMyApiServices.getListIdLastBooks()
        isMockAll = true
    }
    @Test
    fun sendRequestFake() {
        val listBookId = ListBookId()
        listBookId.booksId = arrayListOf(1)
        val listIdParams = ListIdLastBooksParams(1)
        sendRequest(mockRequest, listIdParams, listBookId)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val book = slotHashMapFindBook.captured[bookDefaultResponse.id]
        assert(book!!.id == 1)
    }
}