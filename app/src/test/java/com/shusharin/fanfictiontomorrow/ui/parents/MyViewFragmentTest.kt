package com.shusharin.fanfictiontomorrow.ui.parents

import com.shusharin.fanfictiontomorrow.databinding.FragmentRecyclerViewBinding
import com.shusharin.fanfictiontomorrow.ui.FragmentBaseTest
import com.shusharin.fanfictiontomorrow.ui.main_page.MainPageFragment
import com.shusharin.fanfictiontomorrow.ui.main_page.MainPageViewModel
import io.mockk.mockk
import io.mockk.spyk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class MyViewFragmentTest : FragmentBaseTest() {
    private lateinit var mockMyViewFragment: MyViewFragment<MainPageViewModel, FragmentRecyclerViewBinding>

    @BeforeEach
    fun setUp() {
        mockMyViewFragment = spyk(MainPageFragment(), recordPrivateCalls = true)
        mockkSwipeRefreshLayout(mockMyViewFragment)
    }

    @Test
    fun stopRefresh() {
        isRefreshing = true
        mockMyViewFragment.stopRefresh()
        assert(!isRefreshing)
    }

    @Test
    fun startRefresh() {
        isRefreshing = false
        mockMyViewFragment.startRefresh()
        assert(isRefreshing)
    }
}