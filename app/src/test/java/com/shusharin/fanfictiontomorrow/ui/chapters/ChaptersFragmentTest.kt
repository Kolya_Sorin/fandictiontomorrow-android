package com.shusharin.fanfictiontomorrow.ui.chapters

import com.shusharin.fanfictiontomorrow.ui.main_page.MainPageFragment
import com.shusharin.fanfictiontomorrow.ui.parents.FragmentBaseRecycleTest
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Test

internal class ChaptersFragmentTest : FragmentBaseRecycleTest() {
    private lateinit var mockMainPageFragment: ChaptersFragment

    @BeforeEach
    fun setUp() {
        setReturnNotNullBitmap()
        mockMainPageFragment = spyk(ChaptersFragment(), recordPrivateCalls = true)
        mockkBase(mockMainPageFragment)
        mockkRecycleViewer(mockMainPageFragment)
    }

    @Test
    fun onCreateView() {
        every { bundle.getInt(any()) } returns 3
        mockMainPageFragment.onCreateView(mockk(), mockk(), mockk())
    }
}