package com.shusharin.fanfictiontomorrow.ui.parents

import androidx.viewbinding.ViewBinding
import com.shusharin.fanfictiontomorrow.ui.FragmentBaseTest
import io.mockk.*
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
open class FragmentBaseRecycleTest : FragmentBaseTest() {

}