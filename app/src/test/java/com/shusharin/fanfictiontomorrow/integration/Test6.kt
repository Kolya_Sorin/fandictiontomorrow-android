package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test6: Base() {
    @Test
    fun `Checking the correctness of genre availability`() {
        val presentBookInGenre = getPresentBookInGenre()
        println(presentBookInGenre)
        for (genre in presentBookInGenre) {
            slotHashMapFindBook.clear()
            val bookInGenre = getListIdGenreBooks(genre.genre)
            val books = ArrayList<Book>()
            for (idBookResponse in bookInGenre) {
                books.add(getBookClass(-1, idBookResponse.key))
            }
            books.filter { !it.isDraft }
            println(bookInGenre)
            if (genre.isPresent) {
                Assertions.assertTrue(books.isNotEmpty())
            } else {
                Assertions.assertTrue(books.isEmpty())
            }
        }
    }
}