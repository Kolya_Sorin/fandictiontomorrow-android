package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withContentDescription
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test20 : Base() {
    @Test
    fun checkingTheBackButton() {
        openInDrawerLayout(R.id.nav_settings)
        openInDrawerLayout(R.id.nav_my_library)
        Espresso.pressBack()
        TestCase.assertEquals(
            R.id.nav_settings,
            MainActivity.navController.currentDestination!!.id
        )
    }
}